(function($) {
	$(document).ready(function() {
		$("[name='needDelivery']").change(function() {
			var $this = $(this);
			var needDelivery = $this.val() === 'true' ? true : false;

			if (needDelivery) {
				$(".delivery-block").removeClass('collapse');
			} else {
				$(".delivery-block").addClass('collapse');
			}
		});

		// min-height for Main element
		var windowHeight = $(window).outerHeight(),
			mainElement = $('main'),
			mainElementOffsetTop = mainElement.offset().top,
			footer = $('footer#footer'),
			footerHeight = footer.outerHeight(),
			mainElementMinHeight = (windowHeight - footerHeight - mainElementOffsetTop);

		mainElement.css({ minHeight: mainElementMinHeight });
	});
})(jQuery);